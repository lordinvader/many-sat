module MaxSAT.Exchange where

import NF
import Eval

import MaxSAT.Greedy

import Data.List.Extra
import qualified Data.Set as Set

import System.Random
import Control.Monad.State.Lazy

type Neighbour = PartAssignment -> [PartAssignment]
type StopCondition = Int -> Solution -> Bool
type Perturbation = Int -> PartAssignment -> State StdGen PartAssignment

flipAt :: Int -> PartAssignment -> PartAssignment
flipAt n ass = let (ass1,(v,b):ass2) = splitAt (n-1) ass in
                  ass1 ++ (v,not b) : ass2

flip1 :: Neighbour
flip1 ass = map (\n -> flipAt n ass) [1..Prelude.length ass]

flipN :: Int -> Neighbour
flipN 1 = flip1
flipN n = nub . concat . (map flip1) . (flipN $ n-1)

steepest :: ObjectiveFun -> Solution -> Neighbour -> Solution
steepest _ (bn,bass) _ | bn == Prelude.length bass = (bn,bass)
steepest objf (bn,bass) neighbour =
  let (n,ass) = firstBest $ map (\a -> (objf a,a)) $ neighbour bass in
    if n > bn
    then
      steepest objf (n,ass) neighbour
    else
      (bn,bass)
  where
    firstBest [] = (bn,bass)
    firstBest ((n,ass):xs) = if n > bn then (n,ass) else firstBest xs

buildStop :: Int -> [Clause] -> Int -> Solution -> Bool
buildStop steps cls step (n,_) = step >= steps || n == Prelude.length cls

flipRand :: Perturbation
flipRand n ass = do rs <- Set.fromList <$> sequence (map (const $ extract1 0 $ Prelude.length ass) [1..n])
                    return $ map (f rs) (zip [0..] ass)
  where
    f rs (i,(v,b)) | Set.member i rs = (v,not b)
    f rs (_,a) = a
    extract1 :: Int -> Int -> State StdGen Int
    extract1 a b = do (x,g) <- (uniformR (a,b)) <$> get
                      put g
                      return x

vns :: StopCondition -> Int -> ObjectiveFun -> Solution -> Neighbour -> Perturbation -> Int -> Int -> Int -> State StdGen Solution
vns stop step _ sol _ _ _ _ _ | stop step sol = return sol
vns stop step objf sol neighbour perturbate k0 k dk =
  do ass <- perturbate k $ snd sol  
     let sol' = steepest objf (objf ass, ass) neighbour
     if fst sol' > fst sol
       then vns stop (step+1) objf sol' neighbour perturbate k0 k0 dk
       else let k' = if k+dk > Prelude.length ass then Prelude.length ass else k+dk in
              vns stop (step+1) objf sol neighbour perturbate k0 k' dk
      
