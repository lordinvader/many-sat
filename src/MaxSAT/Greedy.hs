{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
module MaxSAT.Greedy where

import AST
import Eval
import NF

import Data.List.Extra
import qualified Data.Set as Set

type Solution = (Int,PartAssignment)
type ObjectiveFun = PartAssignment -> Int
type GroundSet = PartAssignment

class GreedyBuilder a where
  buildObjf :: [a] -> ObjectiveFun
  buildGroundSet :: [a] -> GroundSet

instance GreedyBuilder Formula where
  buildObjf fs pa = let a = setAll (const Bot) $ map (\(v,b) -> (v,if b then Top else Bot)) pa in
                      Prelude.length $ filter (Top==) $ map (eval a) fs

  buildGroundSet fs = let vs = nub $ concat $ map AST.vars fs in
                        map (\v -> (v,True)) vs ++ map (\v -> (v,False)) vs

instance GreedyBuilder Clause where
  buildObjf cls pa = let spa = Set.fromList pa in
                       Prelude.length $ filter (\c -> c `Set.intersection` spa /= Set.empty) cls
  
  buildGroundSet cls = nub $ concat $ map Set.toList cls

greedy :: ObjectiveFun -> GroundSet -> PartAssignment -> Solution
greedy objf [] sol = (objf sol,sol) 
greedy objf ground sol = let x = maximumOn (\a -> objf $ a:sol) ground in
                           greedy objf (filter (\a -> fst a /= fst x) ground) (x:sol)
