module NF where

import AST
import Data.List
import qualified Data.Set as Set

type Literal = (Variable, Bool)
type Clause = Set.Set Literal

literals :: Formula -> [Literal]
literals = nub . foldAST mf (++) []
  where
    mf (FVar v) = Just [(v,True)]
    mf (Not (FVar v)) = Just [(v,False)]
    mf _ = Nothing

toClauses :: Formula -> [Clause]
toClauses f | isCNF f = foldAST mf (++) [] f
  where
    mf (And _ _) = Nothing
    mf f = Just $ [Set.fromList $ literals f]
toClauses _ = undefined 

vars :: [Clause] -> [Variable]
vars cls = nub $ concat $ map ((map fst).(Set.toList)) cls

isNNF :: Formula -> Bool
isNNF = foldAST mf (&&) True
  where
    mf (Impl _ _) = Just False
    mf (Not f) = Just $ case f of
      FVar _ -> True
      _ -> False
    mf _ = Nothing

isCNF :: Formula -> Bool
isCNF f = isNNF f && foldAST mf (&&) True f
  where
    mf (Or (And _ _) _) = Just False
    mf (Or _ (And _ _)) = Just False
    mf _ = Nothing

toNNF :: Formula -> Formula
toNNF = aux' . aux
  where
    -- Implication Free Normal Form
    aux = mapAST mf
    mf (Impl f1 f2) = Just $ Or (Not $ aux f1) (aux f2)
    mf _ = Nothing
    -- Negation Normal Form
    aux' = mapAST mf'
    mf' (Not (Not f)) = Just $ aux' f
    mf' (Not (And f1 f2)) = Just $ aux' $ Or (Not f1) (Not f2)
    mf' (Not (Or f1 f2)) = Just $ aux' $ And (Not f1) (Not f2)
    mf' (Impl f1 f2) = undefined
    mf' _ = Nothing

toCNF :: Formula -> Formula
toCNF = aux . toNNF
  where
    aux f = if isCNF f
            then
              f
            else
              let f' = mapAST mf f in
                aux f'
    mf (Or f (And f1 f2)) = Just $ And (Or f f1) (Or f f2)
    mf (Or (And f1 f2) f) = Just $ And (Or f f1) (Or f f2)
    mf _ = Nothing

toCNFWeak :: Formula -> Formula
toCNFWeak f = aux (freshVars f) $ toNNF f
  where
    aux (v:vs) f = if isCNF f
                   then
                     f
                   else
                     let f' = mapAST (mf (FVar v)) f in
                       aux vs f'
    mf v f@(Or _ (And f1 f2)) = Just $ And (subst v (And f1 f2) f) (And (Or (Not v) f1) (Or (Not v) f2))
    mf v (Or f1@(And _ _) f2) = mf v $ Or f2 f1
    -- Force recursion in only one branch per time
    mf v (Or f1 f2) = if not $ isCNF f1
                      then
                        Just $ Or (mapAST (mf v) f1) f2
                      else
                        Just $ Or f1 (mapAST (mf v) f2)
    mf v (And f1 f2) = if not $ isCNF f1
                       then
                         Just $ And (mapAST (mf v) f1) f2
                       else
                         Just $ And f1 (mapAST (mf v) f2)
    mf _ _ = Nothing
