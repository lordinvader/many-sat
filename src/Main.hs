module Main where

import AST
import NF

import Input.Formula
import Input.DIMACS

import qualified SAT.DPP as DPP
import qualified SAT.DPLL as DPLL

import qualified MaxSAT.Greedy as Greedy
import qualified MaxSAT.Exchange as Exchange

import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.List


import System.Environment (getArgs)
import System.IO
import System.Random
import Debug.NoTrace
import Control.Monad.State.Lazy

data Entrypoint = Entrypoint { help :: String
                             , parseArgs :: [String] -> Maybe (Map.Map String String)
                             , entryMain :: Map.Map String String -> IO () }

main :: IO ()
main = do args <- getArgs
          if elem "--help" args
            then
            printHelp
            else
            case filter ((Nothing /=).fst) $ map (\ep -> (parseArgs ep args, entryMain ep)) allEntrypoints of
              (Just argsMap,e):_ -> e argsMap
              [] -> printHelp

printHelp :: IO ()
printHelp = let h = unlines $ map help allEntrypoints in
              putStr h

allEntrypoints = [dppEntrypoint, dpllEntrypoint, maxsatEntrypoint]

readingFile :: String -> (String -> IO ()) -> IO ()
readingFile fname f = do fhandle <- openFile fname ReadMode
                         cont <- hGetContents fhandle
                         f cont
                         hClose fhandle

parsingClauses :: String -> ([Clause] -> IO ()) -> IO ()
parsingClauses fname f = readingFile fname $
                         \cont ->
                           case parseClauses $ cont ++ "\n" of
                             Left msg -> putStrLn msg
                             Right cls -> f cls

sortProof = sortOn ((\(Var x _) -> read x :: Int).fst)
                         
dppEntrypoint :: Entrypoint
dppEntrypoint = Entrypoint help parseArgs entryMain
  where
    help = unlines [ "DPP SAT Solver: --dpp <filename>",
                     "  Use the Davis-Putnam Procedure to decide the satisfiability",
                     "  of a given DIMACS formula contained in file <filename>." ]
    parseArgs args =
      if Prelude.length args == 2 && args !! 0 == "--dpp"
      then
        Just $ Map.fromList [("file",args !! 1)]
      else
        Nothing
    entryMain args = case Map.lookup "file" args of
      Nothing -> do putStrLn "Wrong parameters given."
      Just fname ->  parsingClauses fname $
                     \cls ->
                       trace ("** Parsed " ++ (show $ Prelude.length cls) ++ " clauses. **") $
                       let vs = NF.vars cls in
                         let proof = nub $ DPP.dpp vs $ DPP.clean cls in
                           if elem [] proof || Prelude.length proof == 0
                           then
                             do putStrLn "SAT"
                                print $ sortProof $ Set.toList $ DPP.buildAss $ zip vs proof
                           else
                             putStrLn "UNSAT"
        
dpllEntrypoint :: Entrypoint
dpllEntrypoint = Entrypoint help parseArgs entryMain
  where
    help = unlines [ "DPLL SAT Solver: --dpp [-h1 | -h2 | -h3 | -h4] <filename>",
                     "  Use the DPLL Procedure to decide the satisfiability",
                     "  of a given DIMACS formula contained in file <filename>.",
                     "  This implementation can perform parallel branching, to do",
                     "  it pass the runtime arguments in the form '+RTS -Nx -RTS'",
                     "  where 'x' is the number of CPUs to use.",
                     "  Options:",
                     "    -h1  (Default) On branching choose a random variable",
                     "    -h2  On branching choose the most occurring variable",
                     "    -h3  On branching choose the least occurring variable",
                     "    -h4  On branching choose the variable that maximizes the",
                     "         number of unit clauses in the next step"]
    parseArgs args =
      if Prelude.length args >= 2 && args !! 0 == "--dpll"
      then
        Just $ Map.fromList [("file",args !! (Prelude.length args - 1))
                            ,("heuristic",args !! 1)]
      else
        Nothing
    getHeuristic p = case p of
                         "-h1" -> DPLL.firstAvailableVar
                         "-h2" -> DPLL.mostOccurringVar
                         "-h3" -> DPLL.leastOccurringVar
                         "-h4" -> DPLL.lookAheadMaxUnitVar
                         _     -> DPLL.firstAvailableVar
    entryMain args = case (Map.lookup "file" args, Map.lookup "heuristic" args) of
      (Nothing,_) -> putStrLn "Wrong parameters given."
      (Just fname,Just h) -> parsingClauses fname $
                             \cls ->
                               trace ("** Parsed " ++ (show $ Prelude.length cls) ++ " clauses. **") $
                               case DPLL.dpll (getHeuristic h) [] cls of
                                 [] -> putStrLn "UNSAT"
                                 proof ->  do putStrLn "SAT"
                                              print $ sortProof proof

maxsatEntrypoint :: Entrypoint
maxsatEntrypoint = Entrypoint help parseArgs entryMain
  where
    help = unlines [ "Max SAT heuristic Solver: --maxsat <n> <seed> <filename>"
                   , "  Use the Variable Neighbour Search metaheuristic based on"
                   , "  steepest ascent to find a satisfying assignment for the"
                   , "  given DIMACS formula. The numeric parameter <n> determines"
                   , "  the overall number of VNS cycles." ]
    parseArgs args =
      if Prelude.length args == 4 && args !! 0 == "--maxsat"
      then
        Just $ Map.fromList [("file",args !! (Prelude.length args - 1))
                            ,("steps",args !! 1)
                            ,("seed",args !! 2)]
      else
        Nothing
    entryMain args = case ( Map.lookup "file" args
                          , do x <- Map.lookup "steps" args; return (read x :: Int)
                          , do x <- Map.lookup "seed" args; return (read x :: Int) ) of
      (Just fname,Just steps,Just seed) ->
        parsingClauses fname $
        \cls ->
          trace ("** Parsed " ++ (show $ Prelude.length cls) ++ " clauses. **") $
          let clsn = Prelude.length cls
              (n,proof) = Greedy.greedy (Greedy.buildObjf cls) (Greedy.buildGroundSet cls) [] 
              vns = Exchange.vns (Exchange.buildStop steps cls) 0 (Greedy.buildObjf cls) (n,proof) Exchange.flip1 Exchange.flipRand 2 10 2
              (n',proof') = fst $ runState vns $ mkStdGen seed in
                do putStrLn $ "Satisfied " ++ (show n') ++ " clauses over " ++ (show $ clsn)
                   print $ sortProof proof'
      _ -> putStrLn "Wrong parameters given"
