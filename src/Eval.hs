module Eval where

import AST

data Value = Top | Bot
  deriving Eq

instance Show Value where
  show Top = "T" 
  show Bot = "F"

type Assignment = Variable -> Value
type PartAssignment = [(Variable,Bool)]

set :: Assignment -> Variable -> Value -> Assignment
set f v x = f'
  where
    f' v' = if v' == v then x else f v' 

setAll :: Assignment -> [(Variable,Value)] -> Assignment
setAll as [] = as
setAll as ((v,vv):vs) = setAll (set as v vv) vs

eval :: Assignment -> Formula -> Value
eval e f = case f of
  FVar v                      -> e v
  Not f'   | eval e f' == Top -> Bot
  Not f'   | otherwise        -> Top
  And f1 _ | eval e f1 == Bot -> Bot
  And _ f2 | eval e f2 == Bot -> Bot
  And _ _                     -> Top
  Or f1 f2 -> eval e $ Not $ And (Not f1) (Not f2)
  Impl f1 f2 -> eval e $ Or (Not f1) f2
