module AST where

import Data.List

data Variable = Var String Int
  deriving (Ord,Eq)

instance Show Variable where
  show (Var x _) = x

data Formula = FVar Variable
             | Not Formula
             | And Formula Formula
             | Or Formula Formula
             | Impl Formula Formula
  deriving Eq

instance Show Formula where
  show (FVar v) = show v
  show (Not x) = "(¬" ++ show x ++ ")"
  show (And x1 x2) = "(" ++ show x1 ++ " ∧ " ++ show x2 ++ ")"
  show (Or x1 x2) = "(" ++ show x1 ++ " ∨ " ++ show x2 ++ ")"
  show (Impl x1 x2) = "(" ++ show x1 ++ "->" ++ show x2 ++ ")"

mapAST :: (Formula -> Maybe Formula) -> Formula -> Formula
mapAST mf f = case mf f of
  Just f' -> f'
  Nothing -> case f of
    FVar _ -> f
    Not f' -> Not $ mapAST mf f'
    And f1 f2 -> And (mapAST mf f1) (mapAST mf f2)
    Or f1 f2 -> Or (mapAST mf f1) (mapAST mf f2)
    Impl f1 f2 -> Impl (mapAST mf f1) (mapAST mf f2)

foldAST :: (Formula -> Maybe a) -> (a -> a -> a) -> a -> Formula -> a
foldAST mf jf val f = case mf f of
    Just x -> x
    Nothing -> case f of
      FVar _ -> val
      Not f' -> jf (foldAST mf jf val f') val
      And f1 f2 -> jf (foldAST mf jf val f1) (foldAST mf jf val f2)
      Or f1 f2 -> jf (foldAST mf jf val f1) (foldAST mf jf val f2)
      Impl f1 f2 -> jf (foldAST mf jf val f1) (foldAST mf jf val f2)      
  
certificate :: Formula -> [Formula]
certificate x = nub $ case x of
  FVar _ -> [x]
  Not x' -> x:certificate x'
  And x1 x2 -> x:(certificate x1 ++ certificate x2)
  Or x1 x2 -> x:(certificate x1 ++ certificate x2)

depth :: Formula -> Integer
depth = foldAST (const Nothing) jf 0
  where
    jf x y = 1 + max x y

length :: Formula -> Integer
length = foldAST (const Nothing) (+) 1

vars :: Formula -> [Variable]
vars = nub . foldAST mf (++) []
  where
    mf (FVar v) = Just [v]
    mf _ = Nothing

subst :: Formula -> Formula -> Formula -> Formula
subst x x' = mapAST mf
  where
    mf f = if f == x'
           then
             Just x
           else
             Nothing

freshVars :: Formula -> [Variable]
freshVars f = let n = 1 + (maximum $ map (\(Var _ x) -> x) $ vars f) in
  let ns = n : map (1+) ns in
    map (\x -> Var ('_' : show x) x) ns

freshVar :: Formula -> Variable
freshVar = head . freshVars
