module SAT.DPP where

import AST
import NF

import qualified Data.Set as Set
import Data.List

import Control.Parallel
import Debug.NoTrace

(|-) :: Formula -> Formula -> Either Clause [[Clause]]
(|-) f1 f2 = let f = toCNF $ And f1 (Not f2) in
  let cls = clean $ toClauses f in
    let vs = NF.vars cls in
      let proof = nub $ cls : dpp vs cls in
        if elem [] proof
        then Left $ buildAss $ zip vs proof
        else Right proof

buildAss :: [(Variable,[Clause])] -> Clause
buildAss xs = if elem [] $ map snd xs
              then
                foldr f Set.empty xs
              else
                Set.empty
  where
    f (v,cls) ass = (if all (\c -> ass `Set.intersection` c /= Set.empty || ((v,True) `Set.member` c)) cls
                     then (v,True)
                     else (v,False)) `Set.insert` ass                      
 
dpp :: [Variable] -> [Clause] -> [[Clause]]
dpp [] cls = [cls]
dpp _ [] = []
dpp _ cls | any (\c -> Set.null c) cls = [cls]
dpp (v:vs) cls = let stepv = dppStep v cls in
                   let tmsg = (show $ Prelude.length vs) ++ " variables remaining, " ++ (show $ Prelude.length stepv) ++ " clauses." in
                     trace tmsg $ stepv : dpp vs stepv

dppStep :: Variable -> [Clause] -> [Clause]
dppStep p cls = case cls'' of
                  [] -> cls'
                  _ -> clean $ nub $ cls' ++ cls''
  where
    -- p-esonerated
    cls' = filter (\c -> not $ elem p $ NF.vars [c]) cls
    -- p-resolvants
    cls'' = let cs = filter (\c -> elem p $ NF.vars [c]) cls in
      [ Set.filter (\x -> x /= (p,True) && x /= (p,False)) (Set.union c1 c2)
      | c1 <- filter (Set.member (p,True)) cs
      , c2 <- filter (Set.member (p,False)) cs ]

clean :: [Clause] -> [Clause]
clean cls = filter ff cls
  where
    ff :: Clause -> Bool
    ff c = let isb = isBanal c in
             let iss = isSubsunt c in
               not $ isb || iss
    isBanal c = any (\v -> (v,True) `Set.member` c && (v,False) `Set.member` c) $ NF.vars [c]
    isSubsunt c = any (\c1 -> c1 /= c && c1 `Set.isSubsetOf` c) cls
