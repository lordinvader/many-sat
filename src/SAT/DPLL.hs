module SAT.DPLL where

import AST
import NF
import Eval

import Data.List
import Data.List.Extra
import qualified Data.Set as Set

import Control.Parallel
import Debug.NoTrace

type VariableHeuristic = PartAssignment -> [Clause] -> Variable

firstAvailableVar :: VariableHeuristic
firstAvailableVar ass cls = head $ filter (\v -> not $ elem v $ map fst ass) $ NF.vars cls

mostOccurringVar :: VariableHeuristic
mostOccurringVar ass cls = let vs = filter (\v -> not $ elem v $ map fst ass) $ NF.vars cls in
                             maximumOn occurs vs
  where
    occurs :: Variable -> Int
    occurs v = sum $ map (\c -> if Set.member (v,True) c || Set.member (v,False) c then 1 else 0) cls

leastOccurringVar :: VariableHeuristic
leastOccurringVar ass cls = let vs = filter (\v -> not $ elem v $ map fst ass) $ NF.vars cls in
                             minimumOn occurs vs                             
  where
    occurs :: Variable -> Int
    occurs v = sum $ map (\c -> if Set.member (v,True) c || Set.member (v,False) c then 1 else 0) cls

lookAheadMaxUnitVar :: VariableHeuristic
lookAheadMaxUnitVar ass cls = let vs = filter (\v -> not $ elem v $ map fst ass) $ NF.vars cls in
                                maximumOn (\v -> f v True + f v False) vs
  where
    f v b = Prelude.length $ filter (\c -> Set.size c == 1) $ dpllSuss [(v,b)] $ dpllUnit [(v,b)] cls

dpll :: VariableHeuristic -> PartAssignment -> [Clause] -> PartAssignment
dpll _ ass [] = ass
dpll _ _ cls | any (\c -> Set.null c) cls = []
dpll chooseVar ass cls = let tmsg = "Assigned: " ++ (show ass) ++ ", clauses: " ++ (show cls) in
                           trace tmsg $
                           let (ass',cls') = simplified in
                             if cls' == [] || any (\c -> Set.null c) cls
                             then dpll' (ass ++ ass') cls'
                             else case ass' of
                                    [] -> case splitOn $ chooseVar ass cls of
                                            (a1@(_:_),_) -> a1
                                            (_,a2) -> a2
                                    _ -> dpll' (ass ++ ass') cls'
  where
    dpll' = dpll chooseVar
    simplified :: (PartAssignment,[Clause])
    simplified = let (ass',cls') = dpllAssertion [] cls in
                   let ass'' = dpllPure ass' cls' in
                     let cls'' = dpllSuss ass'' $ dpllUnit ass'' cls' in
                       (ass'',cls'')
    splitOn :: Variable -> (PartAssignment,PartAssignment)
    splitOn v = let cls' a = dpllSuss [a] $ dpllUnit [a] cls in
                  let al = (v,True) in
                    let ar = (v,False) in
                      let l = dpll' (al:ass) $ cls' al in
                        let r = dpll' (ar:ass) $ cls' ar in
                          r `par` (l `pseq` (l,r))

dpllAssertion :: PartAssignment -> [Clause] -> (PartAssignment,[Clause])
dpllAssertion ass cls = let res = foldr fa (map Left ass) cls in
                      (nub $ foldr fl [] res, foldr fr [] res)
  where
    fa :: Clause -> [Either (Variable,Bool) Clause] -> [Either (Variable,Bool) Clause]
    fa c res = if Set.size c /= 1
               then (Right c):res
               else let (v,b) = head $ Set.toList c in
                      if elem (Left (v,not b)) res
                      then (Right c):res
                      else (Left (v,b)):res
    fl (Left a) res = a:res
    fl _ res = res
    fr (Right a) res = a:res
    fr _ res = res

dpllPure :: PartAssignment -> [Clause] -> PartAssignment
dpllPure ass cls = foldl fp ass cls
  where
    fp res c = let cls' = filter (c/=) cls in
                 let res' = map fst res in
                   let fp1 v = not $ elem v res' in
                     let fp2 v b = not $ any (\c' -> Set.member (v,not b) c') cls' in
                       res ++ (filter (\(v,b) -> fp1 v && fp2 v b) $ Set.elems c)                     

dpllUnit :: PartAssignment -> [Clause] -> [Clause]
dpllUnit [] cls = cls
dpllUnit _ [] = []
dpllUnit _ cls | any (\c -> Set.null c) cls = cls
dpllUnit ((v,b):as) cls = dpllUnit as $ map fu cls
  where
    fu :: Clause -> Clause
    fu c = Set.filter ((v,not b)/=) c

dpllSuss :: PartAssignment -> [Clause] -> [Clause]
dpllSuss [] cls = cls
dpllSuss _ [] = []
dpllSuss _ cls | any (\c -> Set.null c) cls = cls
dpllSuss ((v,b):as) cls = dpllSuss as $ filter fs cls
  where
    fs :: Clause -> Bool
    fs c = not $ Set.member (v,b) c
