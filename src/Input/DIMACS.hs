module Input.DIMACS(parseClauses) where

import AST
import NF
import Input.Parser
import qualified Data.Set as Set
import Data.Char
import Control.Applicative

spaces = repeat1 $ char ' '

newLines = repeat1 $ char '\n'

endOfClause = char '0'

blanks = spaces <|> newLines

variable :: Parser Variable
variable = do name <- repeat1 $ anyChar isDigit
              return $ Var name 0

literal :: Parser Literal
literal = nlit <|> plit
  where
    plit = do v <- variable
              return (v,True)
    nlit = do char '-'
              v <- variable
              return (v,False)

clause :: Parser Clause
clause = Input.Parser.repeat blanks >>
         ( (endOfClause >> Input.Parser.repeat1 blanks >> return Set.empty) <|>
           (do l <- literal
               Input.Parser.repeat1 blanks
               cls' <- clause
               return $ Set.insert l cls') )

clauses :: Parser [Clause]
clauses = (char 'c' >> Input.Parser.repeat (anyChar ('\n' /=)) >> newLines >> clauses) <|>
          (char 'p' >> Input.Parser.repeat (anyChar ('\n' /=)) >> newLines >> clauses) <|>
          (do c <- clause; cls <- clauses; return $ c:cls) <|>
          (char '%' >> Input.Parser.repeat (anyChar (const True)) >> clauses) <|>
          (return [])
  
parseClauses :: String -> Either String [Clause]
parseClauses s = case apply clauses s of
  ("", Right cls) -> Right cls
  (xs, Right _) -> Left $ "Unexpected \"" ++ xs ++ "\" remaining."
  (xs,Left msg) -> Left $ msg ++ " Found \"" ++ xs ++ "\"." 
