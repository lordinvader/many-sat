--lp/parser.hs
module Input.Formula(parseFormula) where

import AST
import Input.Parser
import Data.Char
import Control.Applicative

word :: Parser String
word = Input.Parser.repeat1 (anyChar isWChar)
  where
    isWChar x = isLetter x || isDigit x

identifier :: Parser Formula
identifier = do name <- word
                return (FVar $ Var name 0)

notOp = do char '!'; return Not

andOp = do char '&'; return And

orOp = do char '|'; return Or

implOp = do char '-'; char '>'; return Impl

binOp i = (if i == 2 then andOp else err "") <|>
          (if i == 1 then orOp else err "") <|>
          (if i == 0 then implOp else err "") <|>
          err "Expected binary operator."

notFormula i = do op <- notOp
                  x <- identifier <|>
                       notFormula i <|>
                       parens (formula 0)
                  return (op x) 

formula' i = do x <- (if i < 3 then formula (i+1) else err "") <|>
                     identifier <|>
                     notFormula i <|>
                     parens (formula 0)
                op <- binOp i
                return $ op x

formula i = (do f <- formula' i; x <- formula i; return $ f x) <|>
            (if i < 3 then formula (i+1) else err "") <|>
            identifier <|>
            notFormula i <|>
            parens (formula 0)

parseFormula :: String -> Either String Formula
parseFormula s = case apply (formula 0) (filter (' '/=) s) of
  ("", Right f) -> Right f
  (xs, Right _) -> Left $ "Unexpected \"" ++ xs ++ "\" remaining."
  (xs,Left msg) -> Left $ msg ++ " Found \"" ++ xs ++ "\"." 
