module Input.Parser where

import Data.Functor
import Control.Applicative

type Error = String
data Parser a = Parser {apply :: String -> (String, Either Error a)}

instance Functor Parser where
  fmap f p = Parser p'
    where
      p' s = case apply p s of
        (s',Right x) -> (s', Right $ f x)
        (s',Left e) -> (s',Left e)

instance Applicative Parser where
  pure x = Parser p
    where
      p s = (s, Right x)

  pf <*> pa = Parser p
    where
      p s = case apply pf s of
        (s', Left e) -> (s', Left e)
        (s', Right f) -> case apply pa s' of
          (s'', Left e) -> (s'', Left e)
          (s'', Right a) -> (s'', Right $ f a)

instance Monad Parser where
  p1 >>= fp2 = Parser p
    where
      p s = case apply p1 s of
        (s', Left e) -> (s', Left e)
        (s', Right x) -> apply (fp2 x) s'

err :: Error -> Parser a
err e = Parser $ \s -> (s,Left e)

instance Alternative Parser where
  empty = err "Unknown"

  p1 <|> p2 = Parser p
    where
      p s = case apply p1 s of
        (s', Right x) -> (s', Right x)
        _ -> apply p2 s

anyChar :: (Char -> Bool) -> Parser Char
anyChar check = Parser p
  where
    p "" = ("", Left "Unexpected end of string.")
    p (c:cs) | check c = (cs,Right c)
    p (c:cs) | otherwise = (c:cs,Left "Unexpected character.")

char :: Char -> Parser Char
char c = anyChar (c ==) <|> (err $ "Expected character '" ++ c:[] ++ "'.")

parens :: Parser a -> Parser a
parens p = do char '('
              x <- p
              char ')'
              return x

repeat :: Parser a -> Parser [a]
repeat p = (do x <- p; xs <- Input.Parser.repeat p; return (x:xs)) <|> pure []                          

repeat1 p = do x <- p; xs <- Input.Parser.repeat p; return (x:xs)
